# Example of using Behat with Mink, Selenium and Google Chrome on Laravel Project
## Get Started  
### Clone repo   
`git clone git@gitlab.com:omordach/behat-mink-laravel.git` # it's just default laravel project   
`cd behat-mink-laravel`  
### Install dependencies  
`composer install` 
### Check Behat version  
`vendor/bin/behat -V`  
### Up locally selenium standalone server, chromedriver, php web server 
(each command in separate terminal window/tab)  
`java -jar selenium/selenium-server-standalone.jar` # selenium standalone server  
`./selenium/chromedriver` # google chrome driver  
`php artisan serve` # run php  web server  
### Run tests  
`vendor/bin/behat`  
You should see something like this:  
<img src="behattestresult.png"
     alt="Behat test result"
     style="float: left; margin-right: 10px;" />

### Need more details?  
Please investigate composer.json and behat.yml files  
Or feel free to use info from these docs:  
[Detailed manual](https://docs.google.com/document/d/1ObgkzvLRV9RfuEpk5ZkIY9VXFYrrNf9DZDp_P3mt1Ig)  
[Test Result](https://youtu.be/X2JliuGeMT0)  
 
