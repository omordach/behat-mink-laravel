@javascript
Feature: Demo
  In order to demonstrate behat test
  As a QA engineer
  I need to be able to open web page and check if needed element is present

  Scenario: Open Laravel default page and find test "Laravel"
    Given I am on "/"
    When I click on News button 
    Then I should be on "https://laravel-news.com/"
    And I should see "laravel news"
